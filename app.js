const express = require('express')
const app = express()
const { User } = require('./models')

app.use('/users', async (req, res) => {
  const users = await User.find().lean().exec()
  res.send({
    data: users,
  })
})

process
  .on('unhandledRejection', (reason, p) => {
    console.log(reason, p)
  })
  .on('uncaughtException', err => {
    console.log(reason, err)
    process.exit(1)
  })

module.exports = app
