const mongoose = require('mongoose')
const config = require('./../config')

mongoose.connect(config.MONGO_URL, { useNewUrlParser: true})

exports.User = mongoose.model('User', require('./User'))