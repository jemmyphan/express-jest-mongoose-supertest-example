const TestHelper = require('../TestHelper')
const userLoader = require('./loaders/userLoader')

const testHelper = new TestHelper()

describe('GET /users', () => {
  beforeAll(async () => {
    await testHelper.start()
  })
  afterAll(async () => {
    await testHelper.stop()
  })
  beforeEach(async () => {
    await userLoader()
  })
  afterEach(async () => {
    await testHelper.clean()
  })

  it('should be success', async () => {
    const response = await testHelper.request
      .get('/users')
      .expect(200)
    expect(response.body.data[0].name).toBe('sate')
  })
})