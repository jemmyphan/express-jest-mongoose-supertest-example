const supertest = require('supertest')
const app = require('./../app')
const mongoose = require('mongoose')

class TestHelper {
  constructor() {
    this.server = null
    this.request = null
  }

  async start() {
    this.server = app.listen()
    this.request = supertest(this.server)
  }

  async stop() {
    this.server.close()
    await mongoose.connection.close()
  }

  async clean() {
    const collections = await mongoose.connection.db.listCollections().toArray()
    await Promise.all(
      collections
        .map(({ name }) => name)
        .map(collection =>
          mongoose.connection.db.collection(collection).drop(),
        ),
    )
  }
}

module.exports = TestHelper
