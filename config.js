const MONGO_PORT = 27017
const MONGO_IP = 'localhost'
const MONGO_DB = 'test'
const MONGO_URL = `mongodb://${MONGO_IP}:${MONGO_PORT}/${MONGO_DB}`

module.exports = {
  MONGO_PORT,
  MONGO_IP,
  MONGO_DB,
  MONGO_URL,
}